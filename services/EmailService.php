<?php
namespace MiniBC\addons\growbehold\services;

use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\connection\exception\ConnectionException;
use MiniBC\core\connection\exception\UnknownConnectionTypeException;
use MiniBC\core\entities\Store;
use MiniBC\core\interfaces\SingletonInterface;
use MiniBC\core\Log;
use MiniBC\core\Mail;
use MiniBC\core\mail\Message;


class OrderService implements SingletonInterface
{
	/** @var OrderService $instance */
	private static $instance;

	/**
	 * returns an instance of this class
	 * @return EmailService
	 */
	public static function getInstance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}
	
}