<?php
namespace MiniBC\addons\growbehold;

use MiniBC\core\Auth;
use MiniBC\core\Config;
use MiniBC\core\controller\ControllerManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\route\Route;

// define('APP_PATH', dirname(__FILE__)."/storefront/app");

/**
 * Router for Affiliate Only Addon
 *
 * @uses MiniBC\core\Auth
 * @uses MiniBC\core\Config
 * @uses MiniBC\core\entities\Addon
 * @uses MiniBC\core\controller\ControllerManager
 *
 * @extends MiniBC\core\route\Route
 */
class AddonRoute extends Route {

	public $basePath = '';
	public $name = '';
	public $label = '';
	public $addon = null;
	
	protected $customerPath;
	/**
	 * setup routes for this addon
	 * @param 	MiniBC\core\entities\Addon 	$addon instance of the addon object
	 */
	public function __construct(Addon $addon) {
		$this->name = $addon->name;
		$this->addon = $addon;

		// setup paths
		$customerBasePath = Config::get('routes::customer');
		$adminBasePath = Config::get('routes::admin');
		$addonBasePath = Config::get('routes::addon');
		$this->basePath = $addonBasePath . '/growbehold';
		$this->customerPath = $customerBasePath . $this->basePath;

		// setup controllers
		$ordersController = ControllerManager::get('Orders@growbehold');

		// Order Controller
		$this->get($this->basePath . '/updateOrder', array($ordersController, 'updateShipStationOrder'));
		$this->post($this->basePath . '/createWebhook', array($ordersController, 'installWebhook'));

	}

	protected function methodNotAllowed() {
		// 405 Method Not Allowed
		http_response_code(405);
		exit;
	}

	protected function routeNotFound() {
		// 404 Not Found
		http_response_code(404);
		exit;
	}
}