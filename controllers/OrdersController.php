<?php
namespace MiniBC\addons\growbehold\controllers;

use \DateTime;
use \DateTimeZone;

use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\Mail;
use MiniBC\core\mail\Message;
use MiniBC\core\EntityFactory;
use MiniBC\bigcommerce\services\StorefrontAssetsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use MiniBC\core\services\exception\WebDAVConnectionFailedException;

class OrdersController
{
    private $db = null;
    private $addon = null;
    private $categories = null;
    private $limit = null;
    private $customer = null;
    private $store = null;
    private $apiConnection = null;
    private $email_template_path;
    private $storecolor;
    private $webDavService = false;

    public function __construct()
    {   
        $this->db = ConnectionManager::getInstance('mysql');
        $this->customer = Auth::getInstance()->getCustomer();
        $this->store = $this->customer->stores[0];  

    }

    public function getOrders()
    {
        // $api = $this->store->getApiConnection(); 
        // $order_530 = $api::getOrder(530);
        // $order_531 = $api::getOrder(531);
        // $orders = $api::getOrders();
        // print_r($api::getLastError());
        // $customerNotes = $order_530->customer_message;
        // $customFields = $this->stringToArrayConverter($customerNotes);
        // print_r($customerNotes);
        // print_r($order_531);
        // print_r($orders);
        $this->pushContentsToShipStation();
    }


    public function installWebhook()
    {   
        $api = $this->store->getApiConnection(); 

        $payload = array( 'store_id' => $this->customer->id );
        $storeToken = JWT::encode($payload, Config::get('site::secret_key'), 'HS512');

        $webhook_order = array(
            'scope' => 'store/order/statusUpdated',
            'headers' => array(
                'x-store-token' => $storeToken
              ),
            'destination' => 'https://staging.minibc.com/apps/growbehold/updateOrder',
            'is_active' => true
        );

        $api::createWebhook($webhook_order);
    }

    public function updateShipStationOrder()
    {  
        // Grab the data from Big Commerce webhook
        $webhookContent = file_get_contents("php://input");
        $webhooks = json_decode($webhookContent, true);
        http_response_code(200);

        $order_id = $webhooks["data"]["id"];   

        $apiKey = 'a1bc9bbea8e045d6852812cd3cc87f94';
        $apiSecret = 'bca7bceb003049c2a45e993932493fd5';

        $rawString = $apiKey . ':' . $apiSecret;
        $authHeader = 'Basic ' . base64_encode($rawString);
  
        $ch = curl_init();
        // $order_id = 431;
        curl_setopt($ch, CURLOPT_URL, "https://ssapi.shipstation.com/orders/?orderNumber=$order_id");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Authorization: $authHeader"
        ));

        $response = curl_exec($ch);

        $data = json_decode($response);
        $orders = $data->orders;
        $order = $orders[0];

        // Process the shipByDate field

        $customFields = $this->stringToArrayConverter($order->customerNotes);

        if (isset($customFields['Buying Club Delivery Date']) || isset($customFields['Home Delivery Date'])) {

            $shipByDateString = isset($customFields['Buying Club Delivery Date']) ? $customFields['Buying Club Delivery Date'] : $customFields['Home Delivery Date'];

            if (strpos($shipByDateString, '(')) {
                $shipByDateString = substr($shipByDateString, 0, strpos($shipByDateString, '('));
            } else {
                $shipByDateString = substr($shipByDateString, 0, strpos($shipByDateString, '*'));    
            }

            $shipByDate = new DateTime("$shipByDateString");
            // $shipByDate->setTimeZone(new DateTimeZone('America/Los_Angeles'));
            $shipByDate = $shipByDate->format('Y-m-d H:i:s');

        } else {

            $requestedShippingService = $order->requestedShippingService;

            $pattern = '/(0[1-9]|1[012]|[1-9])[- \/.](0[1-9]|[12][0-9]|3[01]|[1-9])[- \/.]((?:19|20)\d\d)/';
            preg_match($pattern, $requestedShippingService, $output);
            
            if (isset($output[0])) {
                $timeStamp = $output[0];
                $shipByDate = new DateTime("$timeStamp");
                // $shipByDate->setTimeZone(new DateTimeZone('America/Los_Angeles'));
                $shipByDate = $shipByDate->format('Y-m-d H:i:s');    
            }
        }
    ## Process the customFields

        if ( isset($customFields['Carrier Code']) ) {
            $customField3 = $customFields['Carrier Code'];
            $order->advancedOptions->customField3 = trim(str_replace('*', '', $customField3));
        }

        if (isset($shipByDate)) 
        $order->shipByDate = $shipByDate;

    ## Case of nothing needs to be update, we will exit ou the program
        if (!isset($shipByDate) && !isset($customFields['Carrier Code'])) {
            exit('Nothing needs to be updated');
        }

        $order = json_encode($order);   

        curl_setopt($ch, CURLOPT_URL, "https://ssapi.shipstation.com/orders/createorder");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $order);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: $authHeader"
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        $r = json_decode($response);
        print_r($r);

    }

    /**
    * Collect the information from the customer message and conver the string to an array for futrher processing
    * Assumption: the key-value pairs are seperate by '\n', and the key and the value are seperated by ':'
    */

    public function stringToArrayConverter($string)
    {   
        $rawArray = explode("\n", $string);
        array_pop($rawArray);        
       
        foreach ($rawArray as $field) {
            if (strpos($field, ':')) {
                list($key, $val) = explode(': ', $field);
                $customFields[$key] = $val;
            }
        }

        if (isset($customFields)) {
           return $customFields;     
        } else {
            return null;
        }
        
    }
        
}







